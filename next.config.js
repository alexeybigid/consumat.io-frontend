module.exports = {
  env: {
    PROXIED_API_URL: process.env.PROXIED_API_URL,
  },
  target: "serverless",
};
