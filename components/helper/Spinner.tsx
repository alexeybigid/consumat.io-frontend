import styles from "../../styles/Spinner.module.css";

const Spinner = () => {
  return <span className={styles.spinner}></span>;
};

export default Spinner;
